#!/usr/bin/env mix
# encoding: utf-8
# -*- coding: utf-8 -*-
# vim:set fileencoding=utf-8:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: mix.exs (theopse/metaworld/core/mix.exs)
# Content: Theopse Core Infomation
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Theopse.Core.MixProject do
  use Mix.Project

  def project do
    [
      app: :"Theopse.Core",
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      preferred_cli_env: preferred_cli_env(),
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths, do: ["lib", "src"]
  defp elixirc_paths(:test), do: ["test/support" | elixirc_paths()]
  defp elixirc_paths(_), do: elixirc_paths()

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end

  defp aliases do
    [
      "deps.install": ["deps.get --only #{Mix.env()}", "deps.compile"],
      "compile.strict": [
        "format --dry-run --check-formatted",
        "compile --warnings-as-errors --force"
      ],
      "test.all": [
        "deps.install",
        "compile.strict",
        "test"
      ]
    ]
  end

  defp preferred_cli_env do
    [
      "test.all": :test
    ]
  end
end
