#!/usr/bin/env elixirc
# encoding: utf-8
# -*- coding: utf-8 -*-
# vim:set fileencoding=utf-8:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: core.ex (theopse/metaworld/core/lib/core.ex)
# Content:
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Theopse.Core do
  @moduledoc """
  The most fundamental code library of Theopse, providing the basic
  macro code required.

  Projects except `Theopse.Kernel` and `Theopse.Beyond` will
  use Theopse.Core, with most of them using it through
  `Theopse.Basic`, which is mostly used through
  `Theopse.Standard`.

  `Theopse.Core` is just a regular module, and `Core` only
  represents its position in Theopse.

  For compatibility with future imports, it is recommended to use
  `use Theopse.Core` instead of `import Theopse.Core`, even
  though currently `use Theopse.Core` only does what `import
  Theopse.Core` does.

  use Theopse.Core still supports the except option, as `use
  Theopse.Core, except: [begin: 1]`

  That being said, if there is no necessary reason, it is sufficient to
  import other modules that include `Theopse.Core`, such as
  `Theopse.Standard`.
  """

  @before_compile Theopse.Core.Preuse

  # ---------------------------------------------------------------------------------------------------
  # Define the private types.
  # ---------------------------------------------------------------------------------------------------
  # Refer to `as(_type)` in `Theopse.Basic`.
  # Duplication of definitions should be avoided as the basic library should not depend on more advanced libraries.

  @typedoc false
  @typep as(_type) :: {atom, any, any}

  # ---------------------------------------------------------------------------------------------------
  # Define the guards
  # ---------------------------------------------------------------------------------------------------

  @doc """
  Returns `true` if `term` is an atom; otherwise returns `false`.

  Same as is_atom(term).

  ## Examples

      iex> atom?(false)
      true

      iex> atom?(:name)
      true

      iex> atom?(AnAtom)
      true

      iex> atom?("true")
      false

  """
  @spec atom?(term) :: as(boolean)
  defguard atom?(term) when is_atom(term)

  @doc """
  Returns `true` if `term` is a binary; otherwise returns `false`.

  A binary always contains a complete number of bytes.

  Same as is_binary(term).

  ## Examples

      iex> binary?("foo")
      true

      iex> binary?(<<1::3>>)
      false

  """
  @spec binary?(term) :: as(boolean)
  defguard binary?(term) when is_binary(term)

  @doc """
  Returns `true` if `term` is a bitstring (including a binary); otherwise returns `false`.

  Same as is_bitstring(term).

  ## Examples

      iex> bitstring?("foo")
      true

      iex> bitstring?(<<1::3>>)
      true

  """
  @spec bitstring?(term) :: as(boolean)
  defguard bitstring?(term) when is_bitstring(term)

  @doc """
  Returns `true` if `term` is either the atom `true` or the atom `false` (i.e.,
  a boolean); otherwise returns `false`.

  Same as is_boolean(false).

  ## Examples

      iex> bool?(false)
      true

      iex> bool?(true)
      true

      iex> bool?(:test)
      false

  """
  @spec bool?(term) :: as(boolean)
  defguard bool?(term) when is_boolean(term)

  @doc """
  Returns true if `term` is an exception; otherwise returns `false`.

  Same as is_exception(term).

  ## Examples

      iex> except?(%RuntimeError{})
      true

      iex> except?(%{})
      false

  """
  @spec except?(term) :: as(boolean)
  defguard except?(term) when is_exception(term)

  @doc """
  Returns true if `term` is an exception of `name`; otherwise returns `false`.

  It raises `ArgumentError` if the second element is not a Module.

  Same as is_exception(term, name).

  ## Examples

      iex> except?(%RuntimeError{}, RuntimeError)
      true

      iex> except?(%RuntimeError{}, Macro.Env)
      false

  """
  @spec except?(term, atom) :: as(boolean)
  defguard except?(term, name) when is_exception(term, name)

  @doc """
  Returns true if `term` is an exception; otherwise returns `false`.

  Same as is_exception(term).

  ## Examples

      iex> exception?(%RuntimeError{})
      true

      iex> exception?(%{})
      false

  """
  @spec exception?(term) :: as(boolean)
  defguard exception?(term) when is_exception(term)

  @doc """
  Returns true if `term` is an exception of `name`; otherwise returns `false`.

  It raises `ArgumentError` if the second element is not a Module.

  Same as is_exception(term, name).

  ## Examples

      iex> exception?(%RuntimeError{}, RuntimeError)
      true

      iex> exception?(%RuntimeError{}, Macro.Env)
      false

  """
  @spec exception?(term, atom) :: as(boolean)
  defguard exception?(term, name) when is_exception(term, name)

  @doc """
  Returns `true` if `term` is a floating-point number; otherwise returns `false`.

  Same as is_float(term).

  ## Examples

      iex> float?(1.1)
      true

      iex> float?(1)
      false

  """
  @spec float?(term) :: as(boolean)
  defguard float?(term) when is_float(term)

  @doc """
  Returns `true` if `term` is a function; otherwise returns `false`.

  Same as is_function(term).

  ## Examples

      iex> func?(fn x -> x + x end)
      true

      iex> func?("not a function")
      false

  """
  @spec func?(term) :: as(boolean)
  defguard func?(term) when is_function(term)

  @doc """
  Returns `true` if `term` is a function that can be applied with `arity` number of arguments;
  otherwise returns `false`.

  It raises `ArgumentError` if the second element is not a positive integer.

  Same as is_function(term, arity).

  ## Examples

      iex> func?(fn x -> x * 2 end, 1)
      true

      iex> func?(fn x -> x * 2 end, 2)
      false

  """
  @spec func?(term, non_neg_integer) :: as(boolean)
  defguard func?(term, arity) when is_function(term, arity)

  @doc """
  Returns `true` if `term` is a function; otherwise returns `false`.

  Same as is_function(term).

  ## Examples
      iex> function?(fn x -> x + x end)
      true

      iex> function?("not a function")
      false

  """
  @spec function?(term) :: as(boolean)
  defguard function?(term) when is_function(term)

  @doc """
  Returns `true` if `term` is a function that can be applied with `arity` number of arguments;
  otherwise returns `false`.

  It raises `ArgumentError` if the second element is not a positive integer.

  Same as is_function(term, arity).

  ## Examples

      iex> function?(fn x -> x * 2 end, 1)
      true

      iex> function?(fn x -> x * 2 end, 2)
      false

  """
  @spec function?(term, non_neg_integer) :: as(boolean)
  defguard function?(term, arity) when is_function(term, arity)

  @doc """
  Returns `true` if `term` is an integer; otherwise returns `false`.

  Same as is_integer(term).

  ## Examples

      iex> integer?(1)
      true

      iex> integer?(1.1)
      false

  """
  @spec integer?(term) :: as(boolean)
  defguard integer?(term) when is_integer(term)

  @doc """
  Returns `true` if `term` is a list with zero or more elements; otherwise returns `false`.

  Same as is_list(term).

  ## Examples

      iex> list?([1])
      true

      iex> list?("Not a List")
      false

  """
  @spec list?(term) :: as(boolean)
  defguard list?(term) when is_list(term)

  @doc """
  Returns `true` if `term` is a map; otherwise returns `false`.

  Same as is_map(term).

  ## Examples

      iex> map?(%{a: 1})
      true

      iex> map?("Not a Map")
      false

  """
  @spec map?(term) :: as(boolean)
  defguard map?(term) when is_map(term)

  @doc """
  Returns `true` if `key` is a key in `map`; otherwise returns `false`.

  It raises `BadMapError` if the first element is not a map.

  Same as is_map_key(map, key).

  ## Examples

      iex> Theopse.Core.map_has?(%{a: 1}, :a)
      true

      iex> Theopse.Core.map_has?(%{a: 1}, :b)
      false

  """
  @spec map_has?(map, term) :: as(boolean)
  defguard map_has?(map, key) when is_map_key(map, key)

  @doc """
  Returns `true` if `term` is `nil`, `false` otherwise.

  Same as is_nil(term).

  ## Examples

      iex> nil?(1)
      false

      iex> nil?(nil)
      true

  """
  @spec nil?(term) :: as(boolean)
  defguard nil?(term) when is_nil(term)

  @doc """
  Returns `true` if `term` is either an integer or a floating-point number;
  otherwise returns `false`.

  Same as is_number(term).

  ## Examples

      iex> number?(1)
      true

      iex> number?(1.1)
      true

      iex> number?("Not a Number")
      false
  """
  @spec number?(term) :: as(boolean)
  defguard number?(term) when is_number(term)

  @doc """
  Returns `true` if `term` is a PID (process identifier); otherwise returns `false`.

  Same as is_pid(term).

  ## Examples

      iex> pid?(self())
      true

      iex> pid?(spawn (fn -> exit(:exit) end))
      true

      iex> pid?("Not a PID")
      false

  """
  @spec pid?(term) :: as(boolean)
  defguard pid?(term) when is_pid(term)

  @doc """
  Returns `true` if `term` is a port identifier; otherwise returns `false`.

  Same as is_port(term).

  ## Examples

      iex> begin do
      ...>   port = Port.open({:spawn, "cat"}, [:binary])
      ...>   port?(port)
      ...>   |> tap(fn _ -> send(port, {self(), :close}) end)
      ...> end
      true

      iex> port?(self())
      false

      iex> port?("Not a Port")
      false

  """
  @spec port?(term) :: as(boolean)
  defguard port?(term) when is_port(term)

  @doc """
  Returns `true` if `term` is a reference; otherwise returns `false`.

  Same as is_reference(term).

  ## Examples

      iex> ref?(make_ref())
      true

      iex> ref?("Not a Ref")
      false

  """
  @spec ref?(term) :: as(boolean)
  defguard ref?(term) when is_reference(term)

  @doc """
  Returns `true` if `term` is a reference; otherwise returns `false`.

  Same as is_reference(term).

  ## Examples

      iex> reference?(make_ref())
      true

      iex> reference?("Not a Reference")
      false

  """
  @spec reference?(term) :: as(boolean)
  defguard reference?(term) when is_reference(term)

  @doc """
  Returns true if `term` is a struct; otherwise returns `false`.

  Same as is_struct(term).

  ## Examples

      iex> struct?(URI.parse("/"))
      true

      iex> struct?(%{})
      false

  """
  @spec struct?(term) :: as(boolean)
  defguard struct?(term) when is_struct(term)

  @doc """
  Returns true if `term` is a struct of `name`; otherwise returns `false`.

  It raises `ArgumentError` if the second element is not a Module.

  Same as is_struct(term, name).

  ## Examples

      iex> struct?(URI.parse("/"), URI)
      true

      iex> struct?(URI.parse("/"), Macro.Env)
      false

  """
  @spec struct?(term, atom) :: as(boolean)
  defguard struct?(term, name) when is_struct(term, name)

  @doc """
  Returns `true` if `term` is a tuple; otherwise returns `false`.

  Same as is_tuple(term).

  ## Examples

      iex> tuple?({1})
      true

      iex> tuple?("Not a Tuple")
      false

  """
  @spec tuple?(term) :: as(boolean)
  defguard tuple?(term) when is_tuple(term)

  # ---------------------------------------------------------------------------------------------------
  # Define the macros
  # ---------------------------------------------------------------------------------------------------
  #
  #
  #
  #
  #

  @doc """
  Macro resembling Rust's Result `Ok()`.

  Equals to `:ok`.

  ## Examples

      iex> ok()
      :ok

      iex> ok() = :ok
      :ok

      iex> :ok = ok()
      :ok
  """
  @spec ok() :: :ok
  defmacro ok(), do: :ok

  @doc """
  Macro resembling Rust's Result `Ok(item)`.

  Due to the Elixir convention, the return value is consistent with some(item).

  ## Examples

      iex> ok(0)
      {:ok, 0}

      iex> ok(:foo)
      {:ok, :foo}

      iex> begin do
      ...>  ok(a) = {:ok, :a}
      ...>  a == :a
      ...> end
      true
  """
  @spec ok(any) :: {:ok, any}
  defmacro ok(item), do: {:ok, item}

  @doc """
  Macro resembling Rust's Result `Err()`.

  Equals to `:error`.

  ## Examples

      iex> error()
      :error

      iex> error() = :error
      :error

      iex> :error = error()
      :error

  """
  @spec error() :: :error
  defmacro error(), do: :error

  @doc """
  Macro resembling Rust's Result `Err(item)`.

  ## Examples

      iex> error(:eof)
      {:error, :eof}

      iex> error(:bad_input)
      {:error, :bad_input}

      iex> begin do
      ...>  error(a) = {:error, %RuntimeError{}}
      ...>  a == %RuntimeError{}
      ...> end
      true

  """
  defmacro error(item), do: {:error, item}

  @doc """
  Macro resembling Rust's Option `Some(item)`.

  Following the Elixir convention, it returns {:ok, item} instead of {:some, item}.

  ## Examples

      iex> some(0)
      {:ok, 0}

      iex> some(:foo)
      {:ok, :foo}

      iex> begin do
      ...>  some(a) = {:ok, :a}
      ...>  a == :a
      ...> end
      true

  """
  @spec some(any) :: {:ok, any}
  defmacro some(item), do: {:ok, item}

  @doc """
  Macro resembling Rust's Option `None`.

  Equals to `nil`

  ## Examples

      iex> none()
      nil

      iex> none() = nil
      nil

      iex> :nil = none()
      nil

  """
  @spec none() :: nil
  defmacro none(), do: nil

  #
  #
  #
  #

  @doc """
  Macro resembling Erlang's `begin`.

  ## Examples

      iex> begin do
      ...>  a = 1 + 2
      ...>  a * 4 + 5 - 2
      ...> end
      15

      iex> begin do
      ...>   defmodule Theopse.Core.Test.MultiTest do
      ...>     def test(a, b) when a > b, do: a
      ...>     def test(_, b), do: b
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest.test(1, 4)
      ...> end
      4

      iex> begin do
      ...>   defmodule Theopse.Core.Test.MultiTest2 do
      ...>     use :"Elixir.Theopse.Core"
      ...>     begin do
      ...>       def add(a, b), do: a + b
      ...>     end
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest2.add(1, 3)
      ...> end
      4

  """
  @spec begin(do: any) :: any
  defmacro begin(do: block), do: block

  @doc """
  Macro resembling Erlang's `begin` extending providing `commit`.

  `commit` can be anything you want for it won't be compiled into the binary.
  ## Examples

      iex> begin "This is a commit" do
      ...>  a = 1 + 2
      ...>  a * 4 + 5 - 2
      ...> end
      15

      iex> begin [Theopse.Core.Test.MultiTest, :define] do
      ...>   defmodule Theopse.Core.Test.MultiTest3 do
      ...>     def test(a, b) when a > b, do: a
      ...>     def test(_, b), do: b
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest3.test(1, 4)
      ...> end
      4

      iex> begin do
      ...>   defmodule Theopse.Core.Test.MultiTest4 do
      ...>     use :"Elixir.Theopse.Core"
      ...>     begin {:add, 2} do
      ...>       def add(a, b), do: a + b
      ...>     end
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest4.add(1, 3)
      ...> end
      4

  """
  @spec begin(any, do: any) :: any
  defmacro begin(_commit, do: block), do: block

  @doc """
  Macro that only displays the codes.

  The code inside do block won't be compiled into the binary.

  ## Examples

      iex> left do
      ...>  a = 1 + 2
      ...>  a * 4 + 5 - 2
      ...> end
      nil

      iex> left do
      ...>   defmodule Theopse.Core.Test.MultiTest5 do
      ...>     def test(a, b) when a > b, do: a
      ...>     def test(_, b), do: b
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest5.test(1, 4)
      ...> end
      nil

      iex> left do
      ...>   defmodule Theopse.Core.Test.MultiTest6 do
      ...>     use :"Elixir.Theopse.Core"
      ...>     begin do
      ...>       def add(a, b), do: a + b
      ...>     end
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest6.add(1, 3)
      ...> end
      nil

  """
  @spec left(do: any) :: nil
  defmacro left(do: _block), do: nil

  @doc """
  Macro that only displays the codes and the expects of the code.

  Neither `commit` nor the code inside do block will be compiled into the binary.

  ## Examples

      iex> left "Some Test" do
      ...>  a = 1 + 2
      ...>  a * 4 + 5 - 2
      ...> end
      nil

      iex> left [:deprecated, Theopse.Core.Test.MultiTest7] do
      ...>   defmodule Theopse.Core.Test.MultiTest7 do
      ...>     def test(a, b) when a > b, do: a
      ...>     def test(_, b), do: b
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest7.test(1, 4)
      ...> end
      nil

      iex> left do
      ...>   defmodule Theopse.Core.Test.MultiTest8 do
      ...>     use :"Elixir.Theopse.Core"
      ...>     left {:add, 2, :deprecated} do
      ...>       def add(a, b), do: a + b
      ...>     end
      ...>   end
      ...>
      ...>   Theopse.Core.Test.MultiTest8.add(1, 3)
      ...> end
      nil

  """
  @spec left(any, do: any) :: nil
  defmacro left(_commit, do: _block), do: nil

  # ---------------------------------------------------------------------------------------------------
  # Do init
  # ---------------------------------------------------------------------------------------------------
  #
  #

  @doc false
  defmacro __using__([]) do
    quote do
      import Theopse.Core
      :ok
    end
  end

  defmacro __using__(except: item) do
    quote do
      import Theopse.Core, except: unquote(item)
      :ok
    end
  end
end
