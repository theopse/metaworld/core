#!/usr/bin/env elixirc
# encoding: utf-8
# -*- coding: utf-8 -*-
# vim:set fileencoding=utf-8:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Theopse Organization(mail@theopse.org)
# Licensed under BSD-2-Clause
# File: preuse.ex (theopse/metaworld/core/lib/core/preuse.ex)
# Content:
# Copyright (c) 2023 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Theopse.Core.Preuse do
  @moduledoc """
  The library imported by `Theopse.Core`, compiled before
  `Theopse.Core`.

  This module only provides macros that `Theopse.Core` needs.

  Note: Currently, this library has no effect because the environment
  with `Theopse.Core` imported has already been loaded when
  running doctests.
  """

  @doc """
  See `Theopse.Core.begin/1`.
  """
  @spec begin(do: any) :: any
  defmacro begin(do: block), do: block

  @doc """
  See `Theopse.Core.begin/2`.
  """
  @spec begin(any, do: any) :: any
  defmacro begin(_commit, do: block), do: block

  @doc """
  See `Theopse.Core.left/1`.
  """
  @spec left(do: any) :: nil
  defmacro left(do: _block), do: nil

  @doc """
  See `Theopse.Core.left/2`.
  """
  @spec left(any, do: any) :: nil
  defmacro left(_commit, do: _block), do: nil

  @doc false
  defmacro __before_compile__(_env) do
    if Regex.match?(~r/^.+core\.ex$/, __CALLER__.file) do
      quote do
        import Theopse.Core.Preuse
        :code.delete(Theopse.Core.Preuse)
        :code.purge(Theopse.Core.Preuse)
        :ok
      end
    else
      {:error, :nopermit}
    end
  end
end
